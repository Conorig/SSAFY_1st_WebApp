# Docker?

## 1) 필요성

1) 하나의 서버에 여러 개의 프로그램을 설치하는 것이 문제
(사용하는 library의 버전이 다르거나 / 동일한 port 사용)

-> 차라리 다른 서버에 설치하는 것이 나았고, 그만큼 조립 pc가 늘어남.

---

2) 서버 환경은 계속 바뀜

CentOS -> Ubuntu -> AWS -> Azure

---

3) Devops

개발 주기는 짧아지고, 배포는 자주 일어나고, Microservice Architecture가 유행함.
(프로그램은 잘게 쪼개지고, 관리는 복잡해짐.)

새로운 툴은 계속 나오고 클라우드의 발전으로 설치할 서버가 증가한다.

----



## 2) 정의

: **<u>컨테이너 기반</u>**의 오픈소스 가상화 플랫폼?

**<u>컨테이너</u>** (서버) : 프로그램, 실행환경을 컨테이너로 **<u>추상화</u>**.
동일한 인터페이스 제공으로 프로그램의 배포 및 관리를 **<u>단순화 함</u>**.
(즉 어떤 프로그램 (백엔드, DB 서버, Message Queue 등) 도 컨테이너로 추상화할 수 있고, AWS, Azure, Google Cloud 등 어디서든 사용할 수 있음.)



---

(1) Container 가 무엇인가?

: Container는 격리된 공간에서 process가 동작하는 기술이다.
가상화 기술 중 하나지만, OS를 가상화했던, 기존 방식과는 차이가 있다.
(VMware, VirtualBox : 호스트 OS 위에 게스트 OS를 두어 전체를 가상화하는 방법임.)
(사용법이 간단하나, 무겁고 느려서 운영할 수 없음)

#### 그러나!

CPU의 가상화 기술 HVM을 이용한 KVM과 반가상화 방식의 XEN이 등장함.
게스트 OS가 필요하나 전체 OS를 가상화하는 방식이 아니었으므로, HOST 형 가상화 방식에 비해 성능이 향상되었다.

#### 그러나!

**<u>전가상화든 반가상화는</u>** 추가적인 OS를 설치하여 가상화하는 방법은 성능 문제가 있다

그래서 **<u>프로세스 격리 방식</u>**이 등장..

LINUX는 이 방식을 linux container라고 함.
물론 Docker 이전에도 process를 격리하는 방법이 있었다.

>  리눅스에서는 cgroupscontrol groups와 namespace를 이용한 [LXC](https://linuxcontainers.org/lxc/)Linux container가 있었고 FreeBSD에선 [Jail](https://www.freebsd.org/doc/handbook/jails.html), Solaris에서는 [Solaris Zones](https://docs.oracle.com/cd/E18440_01/doc.111/e18415/chapter_zones.htm#OPCUG426)이라는 기술이 있었습니다. 구글에서는 고오오급 기술자들이 직접 컨테이너 기술을 만들어 사용하였고 [lmctfy(Let Me Contain That For You)](https://github.com/google/lmctfy)라는 ~~뭐라고 읽어야 할지 알 수 없는~~ 오픈소스 컨테이너 기술을 공개했지만 성공하진 못했습니다

----

(2) 이미지
: Image는 **<u>컨테이너에 필요한 파일과 설정값 등을 포함</u>**하고 있는 Immutable 파일.
Container는 Image를 실행한 상태라고 볼 수 있다. (추가, 바뀌는 값은 container에 저장.)



예 ) Ubuntu 이미지는 ubuntu를 실행하기 위한 모든 파일을 가지고 있음.

---

(3) layer

: Docker 이미지는 컨테이너를 실행하기 위한 모든 정보를 가지고 있으므로, 용량이 크다.
docker는 이를 해결하기 위해서 layer라는 개념을 활용하고, 여러 개의 **<u>layer를 하나의 파일 시스템으로 사용</u>**할 수 있게 해줌.

Image는 url 방식으로 관리하고, 태그를 붙일 수 있다.

* Docker는 Image를 만들기 위해 Dockerfile에 자체 DSL (Domain-Specific Language) 언어를 이용하여, Image 생성 과정을 적음.
* Server에 어떤 프로그램을 설치하기 위해 Dependency package를 설치하고, 설정 파일을 만들었던 경험이 있다면, 더 이상 그 과정을 blogging하거나, 메모장에 적지 말고 dockerfile로 관리하면 됨.

참고 : <https://subicura.com/2017/01/19/docker-guide-for-beginners-1.html>